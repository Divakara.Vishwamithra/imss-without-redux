import React, { useState, useEffect } from 'react';

function ScrollComponent(props) {

  const [loadMoreData, setLoadData] = useState(true);

  useEffect(() => {
    getData(loadMoreData);
    setLoadData(false);
  }, [loadMoreData]);

  useEffect(() => {
    const list = document.getElementById('list')
    if(props.scrollable) {   
      list.addEventListener('scroll', (e) => {
        const el = e.target;
        if(el.scrollTop + el.clientHeight === el.scrollHeight) {
          setLoadData(true);
        }
      });  
    } else {  
      window.addEventListener('scroll', () => {
        if (window.scrollY + window.innerHeight === list.clientHeight + list.offsetTop) {
          setLoadData(true);
        }
      });
    }
  }, []);

  useEffect(() => {
    const list = document.getElementById('list');

    if(list.clientHeight <= window.innerHeight && list.clientHeight) {
      setLoadData(true);
    }
  }, [props.state]);


  const getData = (load) => {
    if (load) {
      fetch('https://jsonplaceholder.typicode.com/comments')
        .then(res => {
          return !res.ok 
          ? res.json().then(e => Promise.reject(e)) 
          : res.json();
        })
        .then(res => {
          props.setState([...props.state, ...res]);
        });
    }
  };

  return (
    <ul id='list'>
      { props.state.map((data, i) => <li key={i}>{data.name}</li>) }
    </ul>
  );
};

export default ScrollComponent;
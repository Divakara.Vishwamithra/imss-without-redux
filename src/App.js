import React, { useState } from 'react';
import Scroll from './Components/Scroll';

function App() {
  
  const [data, setData] = useState([]);

  return (
    <div className='App'>
      <Scroll state={data} setState={setData}/>
    </div>
  );
};

export default App;